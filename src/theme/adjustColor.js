import { createTheme } from '@mui/material/styles';

const theme = createTheme({
    palette: {
        primary: {
            main: '#e25854',
            light: '#f5d1d7',
            dark: '#c14849',
            contrastText: '#fff',
        },
        secondary: {
            main: '#7bc9ca',
            light: '#b0dedf',
            dark: '#07a3a2',
            contrastText: '#000',
        },
    },
});

export default theme;